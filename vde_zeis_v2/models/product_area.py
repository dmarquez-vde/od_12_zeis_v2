# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models

class ProductArea(models.Model):
    _name = 'product.area'
    _description = "Product Area"
    _order = 'name'

    name = fields.Char('Area', required=True)
    description = fields.Text(translate=True)
    product_ids = fields.One2many(
        'product.template',
        'product_area_id',
        string='Area Products',
    )
    products_count = fields.Integer(
        string='Number of products',
        compute='_compute_products_count',
    )

    @api.multi
    @api.depends('product_ids')
    def _compute_products_count(self):
        for area in self:
            area.products_count = len(area.product_ids)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    product_area_id = fields.Many2one(
        'product.area',
        string='Area',
        help='Select an Area for this product'
    )
